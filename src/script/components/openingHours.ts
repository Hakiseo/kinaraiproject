import {LitElement, html, customElement, TemplateResult, property} from 'lit-element';

@customElement('opening-hours')
export class MenuSection extends LitElement {
  @property({type: Boolean}) mobileTabletMode: boolean = false;
  @property({type: Boolean}) useHorizontalImage: boolean = false;

  constructor() {
    super();
  }

  render() {
    return html`
      <link rel="preload" as="style" href="global.css">
      <link rel="stylesheet" href="global.css">
      <link rel="preload" as="style" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">      
      <div id="hoursSection" class="hoursSection">
        <div class="hours">
          <h2> Åbningstider: </h2>
          <p> Mandag: 12:00 - 22:00 </p>
          <p> Tirsdag: 12:00 - 22:00 </p>
          <p> Onsdag: 12:00 - 22:00 </p>
          <p> Torsdag: 12:00 - 22:00 </p>
          <p> Fredag: 12:00 - 22:00 </p>
          <p> Lørdag: 12:00 - 22:00 </p>
          <p> Søndag: Lukket </p>

          <p> <i class="fas fa-map-marker-alt" style="padding-right: 5px"></i> Gasværksvej 23, 1650 København V </p>
          <p> <i class="fas fa-phone" style="padding-right: 5px"></i> Tlf. +45 33 25 60 25 & +45 33 25 62 25 </p>
        </div>
        <div class="hoursImageSection">
          ${this.renderHoursImage()}
        </div>
      </div>
    `;
  }

  renderHoursImage(): TemplateResult | void {
    if (this.useHorizontalImage) return html `
      <img src="/assets/images/KinAraiImage-1920.jpg" loading="lazy" alt="Buddha statue portrait" class="contactSideImage">
    `;
    if (this.mobileTabletMode) return html `
      <img src="/assets/images/KinAraiImage-768.jpg" loading="lazy" alt="Buddha statue portrait" class="contactSideImage">
    `
    return html `
      <img src="/assets/images/buddhaSmall-700.jpg" loading="lazy" alt="Buddha statue side portrait" class="contactSideImage">
    `
  }
}
