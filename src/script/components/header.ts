import { LitElement, html, customElement, property } from 'lit-element';

@customElement('app-header')
export class AppHeader extends LitElement {

  @property({type: Boolean}) mobileTabletMode: boolean = false;
  @property({type: Boolean}) showBurgerMenu: boolean = false;

  constructor() {
    super();
  }

  toggleBurgerMenu() {
    this.showBurgerMenu = !this.showBurgerMenu;
  }

  sendAndToggle(section: string) {
    this.sendScrollEvent(section)
    this.toggleBurgerMenu()
  }

  render() {
    if (this.mobileTabletMode) return html`
      <link rel="preload" as="style" href="global.css">
      <link rel="stylesheet" href="global.css">
      <link rel="preload" as="style" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">

      <ul style="justify-content: end;">
        <li>
          <a @click="${() => this.toggleBurgerMenu()}"> <i class="fas fa-bars"></i> </a>
        </li>
        ${this.showBurgerMenu ? html `
        <li class="burgerItem"> <a @click="${() => this.sendAndToggle("home")}"> Home </a> </li>
        <li class="burgerItem"> <a @click="${() => this.sendAndToggle("soups")}"> Supper </a> </li>
        <li class="burgerItem"> <a @click="${() => this.sendAndToggle("starters")}"> Forretter</a> </li>
        <li class="burgerItem"> <a @click="${() => this.sendAndToggle("mains")}"> Hovedretter </a> </li>
        <li class="burgerItem"> <a @click="${() => this.sendAndToggle("extras")}"> Tilbehør </a> </li>
        <li class="burgerItem"> <a @click="${() => this.sendAndToggle("hours")}"> Åbningstider & kontakt </a> </li>        
        ` : ""}
      </ul>
    `

    return html`
      <link rel="stylesheet" href="global.css">
      
      <ul>
        <li> <a @click="${() => this.sendScrollEvent("home")}"> Home </a> </li>
        <!-- <li> <a @click="${() => this.sendScrollEvent("menus")}"> Menuer </a> </li> -->
        <li> <a @click="${() => this.sendScrollEvent("soups")}"> Supper </a> </li>
        <li> <a @click="${() => this.sendScrollEvent("starters")}"> Forretter</a> </li>
        <li> <a @click="${() => this.sendScrollEvent("mains")}"> Hovedretter </a> </li>
        <li> <a @click="${() => this.sendScrollEvent("extras")}"> Tilbehør </a> </li>
        <li> <a @click="${() => this.sendScrollEvent("hours")}"> Åbningstider & kontakt </a> </li>
      </ul>
    `;
  }

  sendScrollEvent(section: string) {
    this.dispatchEvent(new CustomEvent("scrollTo", {detail: section}))
  }
}
