export interface MenuItemInterface {
    productID: number,
    productThaiName: string,
    productLatinName: string,
    productDescriptionDanish: string,
    productDescriptionEnglish: string,
    productPrice: number,
    spiceLevel?: number,
    pieces?: string
}

export interface DessertInterface {
    productID: number,
    productLatinName: string,
    productDescriptionDanish: string,
    productDescriptionEnglish: string,
    productPrice: number,
}

export interface AccessoriesInterface {
    productLatinDanish: string,
    productLatinEnglish: string,
    price: number
}
