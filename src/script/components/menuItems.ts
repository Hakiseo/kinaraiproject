import {AccessoriesInterface, DessertInterface, MenuItemInterface} from "./menuItemInterface";

export const soups: MenuItemInterface[] =
    [
        {
            productID: 1,
            productThaiName: "ต้มยำกุ้ง",
            productLatinName: "Tom Yam Gung",
            spiceLevel: 1,
            productDescriptionDanish: "Stærk suppe med kongerejer, galangarod, citrongræs, champignons, lime, chili og koriander",
            productDescriptionEnglish: "Spicy soup with king prawns, galangal root, lemongrass, mushrooms, lime, chili and coriander",
            productPrice: 59,
        },
        {
            productID: 2,
            productThaiName: "ต้มยำไก่ ",
            productLatinName: "Tom Yam Gai",
            spiceLevel: 1,
            productDescriptionDanish: "Stærk suppe med kylling, galangarod, citronsgræs, champignons, lime, chili og koriander",
            productDescriptionEnglish: "Spicy soup with chicken, galangal root, lemongrass, mushrooms, lime, chili and coriander",
            productPrice: 49,
        },
        {
            productID: 3,
            productThaiName: "ต้มยำทะเล",
            productLatinName: "Tom Yam Talay",
            spiceLevel: 1,
            productDescriptionDanish: "Stærk suppe med skaldyr, galangarod, citronsgræs, champignons, lime, chili og koriander",
            productDescriptionEnglish: "Spicy soup with seafood, galangal root, lemongrass, mushrooms, lime, chili and coriander",
            productPrice: 59,
        },
        {
            productID: 4,
            productThaiName: "ต้มข่าไก่",
            productLatinName: "Tom Kha Kai",
            productDescriptionDanish: "Kokosmælk suppe med kylling, galangarod, citrongræs, champignons, lime, chili og koriander",
            productDescriptionEnglish: "Coconut milk soup with chicken, galangal root, lemongrass, mushrooms, lime, chili and coriander",
            productPrice: 49,
        },
        {
            productID: 5,
            productThaiName: "ต้มข่ากุ้ง",
            productLatinName: "Tom Kha Gung",
            productDescriptionDanish: "Kokosmælk suppe med rejer, galangarod, citrongræs, champignons, lime, chili og koriander",
            productDescriptionEnglish: "Coconut milk soup with king prawns, galangal root, lemongrass, mushrooms, lime, chili and coriander",
            productPrice: 59,
        },
        {
            productID: 6,
            productThaiName: "ต้มข่าทะเล",
            productLatinName: "Tom Kha Talay",
            productDescriptionDanish: "Kokosmælk suppe med skaldyr, galangarod, citrongræs, champignons, lime, chili og koriander",
            productDescriptionEnglish: "Coconut milk soup with seafood, galangal root, lemongrass, mushrooms, lime, chili and coriander",
            productPrice: 59,
        },
        {
            productID:7,
            productThaiName: "ต้มจืดเจ",
            productLatinName: "Tom Juet Je",
            productDescriptionDanish: "Vegetar suppe med glasnudler, tofu og grøntsager",
            productDescriptionEnglish: "Vegetarian soup with glass noodles, tofu and vegetables",
            productPrice: 49,
        },
    ]

export const firstCourses: MenuItemInterface[] =
    [
        {
            productID: 8,
            productThaiName:"ไก่สะเต๊ะ",
            productLatinName: "Kai Satay",
            pieces: "(4 stk./ 4 pcs.)",
            productDescriptionDanish: "Marineret kyllingesoyd serveret med jordnøddesauce",
            productDescriptionEnglish: "Marinated chicken satay served with peanut sauce",
            productPrice: 69,
        },
        {
            productID: 9,
            productThaiName:"ปอเปี๊ยะทอด",
            productLatinName:"Paw Pia Thowt",
            pieces: "(4 stk./ 4 pcs)",
            productDescriptionDanish: "Hjemmelavet Thai forårsruller med kylling serveret med sød chilisauce",
            productDescriptionEnglish: "Homemade Thai spring rolls stuffed with chicken and served with sweet chili sauce",
            productPrice: 59,
        },
        {
            productID: 10,
            productThaiName:"ปอเปี๊ยะเจ",
            productLatinName:"Paw Pia Je",
            pieces: "(4 stk./ 4 pcs)",
            productDescriptionDanish: "Hjemmelavet Thai vegetar forårsruller med grønsager serveret med sød chilisauce",
            productDescriptionEnglish: "Homemade Thai vegetarian spring rolls stuffed with vegetables and served with sweet chili sauce",
            productPrice: 59,
        },
        {
            productID: 11,
            productThaiName:"กุ้งชุบแป้งทอด",
            productLatinName:"Gung Chup Paeng Thowt",
            pieces:"(5 stk./ 5 pcs)",
            productDescriptionDanish: "Dybstegt tempura rejer serveret med sød chilisauce",
            productDescriptionEnglish: "Crispy tempura shrimp served with sweet chili sauce",
            productPrice: 79,
        },
        {
            productID: 12,
            productThaiName:"ทอดมันปลา",
            productLatinName:"Thowt Man Plaa ",
            spiceLevel: 1,
            pieces: "(4 stk./ 4 pcs)",
            productDescriptionDanish: "Thai fiskefrikadeller med chili, rød karrypasta, lange bønner serveret med sød chilisauce",
            productDescriptionEnglish: "Thai fish-cakes made with a mixture of chili, red curry paste, long beans and served with sweet chili sauce",
            productPrice: 79,
        },
        {
            productID: 13,
            productThaiName:"ทอดมันกุ้ง",
            productLatinName:"Thowt man Gung",
            pieces: "(4 stk./ 4 pcs)",
            productDescriptionDanish: "Thai rejefrikadeller serveret med blommesauce",
            productDescriptionEnglish: "Thai shrimp-cakes served with plum sauce",
            productPrice: 100,
        },
        {
            productID: 14,
            productThaiName:"ข้าวเกรียบกุ้ง",
            productLatinName:"Khao Kreab Gung",
            productDescriptionDanish: "Thai rejechips",
            productDescriptionEnglish: "Thai shrimp chips",
            productPrice: 45,
        },
        {
            productID: 15,
            productThaiName:"ไก่ห่อใบเตย",
            productLatinName:"Kai Bai Teay",
            pieces: "(4 stk./ 4 pcs)",
            productDescriptionDanish: "Marineret kylling I pandan blade serveret med sød chilisauce",
            productDescriptionEnglish: "Marinated chicken in pandan leafs, served with sweet chili sauce",
            productPrice: 79,
        },
    ]

export const mainCourses: MenuItemInterface[] =
    [
        {
            productID: 20,
            productThaiName: "ผัดเผ็ด",
            productLatinName: "Pad Phet",
            spiceLevel: 3,
            productDescriptionDanish: "Rød karrypasta stegt med sød basilikum, limeblade, lange bønner, bambusskud, peberfrugt, thai aubergine og (Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / laks +20,-)",
            productDescriptionEnglish: "Red curry paste fried with sweet basil, lime leaves, thai eggplant, bambooshoots, long beans and ( Chicken / Beef / Pork / Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / salmon +20,-)",
            productPrice: 135,
        },
        {
            productID: 21,
            productThaiName: "ผัดขิง",
            productLatinName: "Pad King",
            productDescriptionDanish: "Stegte grønsager med ingefær og ( Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / laks +20,-)",
            productDescriptionEnglish: "Fried vegetables with ginger and ( Chicken / Beef / Pork / Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / salmon +20,-)",
            productPrice: 125,
        },
        {
            productID: 22,
            productThaiName: "ผัดเม็ดมะม่วงหิมพานต์",
            productLatinName: "Pad Met MaMuang",
            productDescriptionDanish: "Stegte grønsager med ananas, cashewnødder og ( Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / laks +20,-)",
            productDescriptionEnglish: "Fried vegetables with pineapple, cashews and ( Chicken / Beef / Pork / Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / salmon +20,-)",
            productPrice: 125,
        },
        {
            productID: 23,
            productThaiName: "ผัดผัก",
            productLatinName: "Pad Pak",
            productDescriptionDanish: "Stegte grønsager i østerssauce med ( Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / laks +20,- )",
            productDescriptionEnglish: "Fried vegetables in oyster sauce with ( Chicken / Beef / Pork / Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / salmon +20,-)",
            productPrice: 125,
        },
        {
            productID: 24,
            productThaiName: "ผัดพริกกระเพรา",
            productLatinName: "Pad Prik Ka Phrao",
            spiceLevel: 2,
            productDescriptionDanish: "Stegte grønsager med hvidløg, chili, basilikum og ( Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / laks +20,-)",
            productDescriptionEnglish: "Fried vegetables with garlic, chili, basil and ( Chicken / Beef / Pork / Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / salmon +20,-)",
            productPrice: 125,
        },
        {
            productID: 25,
            productThaiName: "ผัดเปรี้ยวหวาน",
            productLatinName: "Pad Priew Wan",
            productDescriptionDanish: "Stegte grønsager i sur-sødsauce med ananas og ( Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / laks +20,- )",
            productDescriptionEnglish: "Fried vegetables in sweet and sour sauce, pineapple with ( Chicken / Beef / Pork / Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / salmon +20,-)",
            productPrice: 125,
        },

        ]

export const specialDuckDishes: MenuItemInterface [] =
    [
        {
            productID: 27,
            productThaiName: "เป็ดสามรส",
            productLatinName: "Pet Sam Rod",
            productDescriptionDanish: "And stegt i rød karrypasta, sur-sødsauce, basilikum, grønsager, cashewnødder og ananas",
            productDescriptionEnglish: "Roasted duck in red curry paste, sweet and sour sauce, basil, vegetables, cashews and pineapple",
            productPrice: 145,

        },
        {
            productID: 28,
            productThaiName: "เป็ดมะขาม",
            productLatinName: "Pet Ma Kram",
            productDescriptionDanish: "And stegt i tamarind sauce, grønsager og sesamfrø",
            productDescriptionEnglish: "Roasted duck in tamarin sauce, vegetables and sesam seeds",
            productPrice: 145,

        },
        {
            productID: 29,
            productThaiName: "เป็ดผัดกระเพรากรอบ",
            productLatinName: "Pet Yang Ka Phrao Krab",
            spiceLevel: 2,
            productDescriptionDanish: "Stegt and med hvidløg, chili og basilikum",
            productDescriptionEnglish: "Roasted duck with garlic, chili and basil",
            productPrice: 145,

        },
    ]

export const specialFriedPork: MenuItemInterface [] =
    [
        {
            productID: 30,
            productThaiName: "ผัดคะน้าหมูกรอบ",
            productLatinName: "Pad Pak Ka Na Moo Krob",
            spiceLevel: 3,
            productDescriptionDanish: "Friturestegt flæsk med thai broccoli, hvidløg og chili",
            productDescriptionEnglish: "Spicy fried crispy pork with thai broccoli, garlic and chili",
            productPrice: 155,

        },
        {
            productID: 31,
            productThaiName: "ผัดกระเพราหมูกรอบ",
            productLatinName: "Pad Prik Ka Phrao Moo Krob",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk friturestegt stegt flæsk med hvidløg, chili og basilikum",
            productDescriptionEnglish: "Crispy deep-fried pork with garlic, chili and holly basil leaves",
            productPrice: 155,

        },
        {
            productID: 32,
            productThaiName: "ผัดเผ็ดหมูกรอบ",
            productLatinName: "Pad Phet Moo Krob",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk friturestegt stegt flæsk i rød karrypasta med sød basilikum, limeblade, bambusskud, peberfrugt, thai aubergine",
            productDescriptionEnglish: "Crispy deep-fried spicy pork in red curry paste with sweet basil, lime leaves, thai eggplant, bamboo shoots",
            productPrice: 155,

        },
        {
            productID: 33,
            productThaiName: "ผัดพริกขิงหมูกรอบ",
            productLatinName: "Pad Prik King Moo Krob",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk friturestegt flæsk i rød karrypasta med sød basilikum, limeblade, lange bønner og chili",
            productDescriptionEnglish: "Crispy deep-fried spicy pork in red curry paste with sweet basil, lime leaves, long beans and chili",
            productPrice: 155,

        },
    ]

export const curryDishes: MenuItemInterface [] =
    [
        {
            productID: 34,
            productThaiName: "แกงเผ็ด",
            productLatinName: "Gaeng Phet",
            spiceLevel: 2,
            productDescriptionDanish: "Stærk rød karry lavet på kokosmælk med bambusskud, basilikum og ( Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / Laks +20,- )",
            productDescriptionEnglish: "Spicy red curry made on coconut milk with bamboo shoots, sweet basil and ( Chicken / Beef / Pork / Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / Salmon +20,-)",
            productPrice: 135,

        },
        {
            productID: 35,
            productThaiName: "แกงเขียวหวาน",
            productLatinName: "Gaeng Khiew Wan",
            spiceLevel: 2,
            productDescriptionDanish: "Stærk grøn karry lavet på kokosmælk med bambusskud, basilikum, limeblade, thai aubergine og ( Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / Laks +20,-)",
            productDescriptionEnglish: "Spicy green curry made on coconut milk with bamboo shoots, sweet basil, lime leaves, thai eggplant and ( Chicken / Beef / Pork / Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / Salmon +20,-)",
            productPrice: 135,

        },
        {
            productID: 36,
            productThaiName: "แกงพะแนง",
            productLatinName: "Gaeng Phanaeng",
            spiceLevel: 1,
            productDescriptionDanish: "Mild panang karry lavet på kokosmælk med sød basilikum, grønsager og ( Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / Laks +20,- )",
            productDescriptionEnglish: "Mild phanaeng curry made on coconut milk with sweet basil, vegetables and ( Chicken / Beef / Pork /  Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / Salmon +20,-)",
            productPrice: 135,

        },
        {
            productID: 37,
            productThaiName: "แกงมัสมั่น",
            productLatinName: "Gaeng Massaman",
            productDescriptionDanish: "Massaman karry lavet på kokosmælk med kartofler, løg, cashewnødder og ( Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / Laks +20,- )",
            productDescriptionEnglish: "Massaman curry made on coconut milk with potatoes, onions, cashews and ( Chicken / Beef / Pork / Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / Salmond +20,-)",
            productPrice: 135,

        },
        {
            productID: 38,
            productThaiName: "แกงกระหรี่",
            productLatinName: "Gaeng Kari",
            productDescriptionDanish: "Gul karry lavet på kokosmælk med kartofler, løg, gulerødder, cashewnødder og ( Kylling / Oksekød / svinekød / Tofu / And +20,- / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- / Laks +20,- )",
            productDescriptionEnglish: "Yellow curry made on coconut milk with potatoes, onions, carrots, cashews and ( Chicken / Beef / Pork / Tofu / Duck +20,- / Shrimps +20,- / Squid +20,- / Seafood +20,- / Salmon +20,-)",
            productPrice: 135,

        },
    ]

export const friedRice: MenuItemInterface [] =
    [
        {
            productID: 39,
            productThaiName: "ข้าวผัด",
            productLatinName: "Khao Pad",
            productDescriptionDanish: "Stegte ris med æg, tomater, gulerødder, løg, blomkål, broccoli og ( Kylling  / Oksekød / Svinekød / Tofu / Rejer +30,-)",
            productDescriptionEnglish: "Stir fried rice with eggs, tomatoes, carrots, onions, broccoli and ( Chicken / Beef / Pork / Prawns +30,-)",
            productPrice: 120,

        },
    ]

export const noodles: MenuItemInterface [] =
    [
        {
            productID: 40,
            productThaiName: "ผัดไทย",
            productLatinName: "Pad Thai",
            productDescriptionDanish: "Stegte risnudler med æg, bønnespirer, peanuts, kow choi og ( Kylling / Oksekød / svinekød / Tofu / Rejer +30,- / Blæksprutte +30,- / Skaldyr +30,- )",
            productDescriptionEnglish: "Stir fried rice noodles with eggs, bean sprouts, peanuts, Chinese leek and ( Chicken / Beef / Pork / Tofu / Shrimps +30,- / Squid +30,- / Seafood +30,-)",
            productPrice: 120,

        },
        {
            productID: 41,
            productThaiName: "ผัดไทยวุ้นเส้น",
            productLatinName: "Pad Thai Wun Sen",
            productDescriptionDanish: "Stegte glasnudler med æg, bønnespirer, peanuts, kow choi og ( Kylling / Oksekød / svinekød / Tofu / Rejer +30,- / Blæksprutte +30,- / Skaldyr +30,- )",
            productDescriptionEnglish: "Stir fried vermicelli noodles with eggs, bean sprouts, peanuts, Chinese leek and ( Chicken / Beef / Pork / Tofu / Shrimps +30,- / Squid +30,- / Seafood +30,-)",
            productPrice: 120,

        },
        {
            productID: 42,
            productThaiName: "ผัดซีอิ๊ว",
            productLatinName: "Pad Seew",
            productDescriptionDanish: "Stegte brede risnudler med æg, gulerødder, broccoli, kål og ( Kylling / Oksekød / svinekød / Tofu / Rejer +30,- / Blæksprutte +30,- / Skaldyr +30,- )",
            productDescriptionEnglish: "Stir fried wide rice noodles with eggs, carrots, broccoli, cabbage and ( Chicken / Beef / Pork / Tofu / Shrimps +30,- / Squid +30,- / Seafood +30,-)",
            productPrice: 120,

        },
        {
            productID: 43,
            productThaiName: "ผัดก๋วยเตี๋ยวขี้เมา",
            productLatinName: "Pad Kee Mauw",
            spiceLevel: 2,
            productDescriptionDanish: "Stegte bredde risnudler med chili, æg, grønsager og ( Kylling / Oksekød / svinekød / Tofu / Rejer +30,- / Blæksprutte +30,- / Skaldyr +30,- )",
            productDescriptionEnglish: "Stir fried wide rice noodles with chili, eggs, vegetables and ( Chicken  /  Beef  /  Pork  / Tofu  /  Shrimps +30,- / Squid +30,- / Seafood +30,-)",
            productPrice: 120,

        },
        {
            productID: 44,
            productThaiName: "ผัดหมี่ไข่",
            productLatinName: "Pad Mee",
            productDescriptionDanish: "Stegte æggenudler med grønsager, bønnespirer og ( Kylling / Oksekød / svinekød / Tofu / Rejer +30,- / Blæksprutte +30,- / Skaldyr +30,- )",
            productDescriptionEnglish: "Stir fried egg noodles with vegetables, bean sprouts and (   Chicken  /  Beef  /  Pork  /  Tofu  /   Shrimps +30,- / Squid +30,- / Seafood +30,-)",
            productPrice: 120,

        },
    ]

export const thaiSalad: MenuItemInterface [] =
    [
        {
            productID: 45,
            productThaiName: "ยำเนื้อย่าง",
            productLatinName: "Yam Neua",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk autentisk thai salat med grillet okseculotte, frisk chili, lime og koriander",
            productDescriptionEnglish: "Spicy authentic thai salad with roasted beef, fresh chili, lime and",
            productPrice: 155,

        },
        {
            productID: 46,
            productThaiName: "ยำไก่",
            productLatinName: "Yam Kai",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk autentisk thai kyllingsalat med frisk chili, lime og koriander",
            productDescriptionEnglish: "Spicy authentic thai chicken salad with fresh chili, lime and coriander",
            productPrice: 130,

        },
        {
            productID: 47,
            productThaiName: "ยำวุ้นเส้นทะเล",
            productLatinName: "Yam Wun Sen Talay",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk autentisk thai skaldyrsalat med glasnudler med friske chili, lime og koriander",
            productDescriptionEnglish: "Spicy authentic thai seafood salad with vermicelli noodles, fresh chili, lime coriander",
            productPrice: 155,

        },
        {
            productID: 48,
            productThaiName: "ส้มตำ",
            productLatinName: "Som Tam",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk autentisk thai papaya salat med tørret rejer, lange bønner, peanuts, lime og frisk chili",
            productDescriptionEnglish: "Spicy authentic thai papaya salad with dry shrimps, long beans, peanuts, lime and fresh chili",
            productPrice: 145,

        },
    ]

export const specialDishes: MenuItemInterface [] =
    [
        {
            productID: 49,
            productThaiName: "ลาบ",
            productLatinName: "Lab",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk hakket ( Kylling / Oksekød / Svinekød ) med chili, lime og koriander",
            productDescriptionEnglish: "Spicy Chopped ( Chicken / Beef / Pork ) with chili, lime and coriander ",
            productPrice: 145,

        },
        {
            productID: 50,
            productThaiName: "เนื้อน้ำตก",
            productLatinName: "Neua Nam Tok",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk grillet okseculotte med chili, lime og koriander",
            productDescriptionEnglish: "Spicy grilled roast beef with chili, lime and coriander",
            productPrice: 155,

        },
        {
            productID: 51,
            productThaiName: "หมูน้ำตก",
            productLatinName: "Moo Nam Tok",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk grillet svinekød med chili, lime og koriander",
            productDescriptionEnglish: "Spicy grilled pork with chili, lime and coriander",
            productPrice: 145,

        },
        {
            productID: 52,
            productThaiName: "ผัดผักบุ้งไฟแดง",
            productLatinName: "Pad Pak Bung Fai Daeng",
            spiceLevel: 3,
            productDescriptionDanish: "Stegt flæsk med thai vandspinat, hvidløg og chili",
            productDescriptionEnglish: "Fried pork with water spinach, garlic and chili",
            productPrice: 155,

        },
        {
            productID: 53,
            productThaiName: "ปีกไก่ทอดกระเทียมพริกไทย",
            productLatinName: "Peak Kai Tod Kra Thiem",
            productDescriptionDanish: "Friturestegte kyllingevinger med ristet hvidløg og peber",
            productDescriptionEnglish: "Deep fried chicken wings with roasted garlic and pepper",
            productPrice: 135,

        },
        {
            productID: 54,
            productThaiName: "กระดูกหมูทอดกระเทียม",
            productLatinName: "Kra Duk Moo Tod Kra Thiem",
            productDescriptionDanish: "Friturestegte spareribs med ristet hvidløg og peber",
            productDescriptionEnglish: "Deep fried spareribs with roasted garlic and pepper",
            productPrice: 135

        },
        {
            productID: 55,
            productThaiName: "ปูผัดผงกะหรี่",
            productLatinName: "Poo Pad Punk Kari",
            productDescriptionDanish: "Stegt krabbe i gul karry",
            productDescriptionEnglish: "Stir-fried crab meat in yellow curry",
            productPrice: 250,

        },
        {
            productID: 56,
            productThaiName: "ต้มแซบกระดูกหมู",
            productLatinName: "Tom Saab",
            spiceLevel: 3,
            productDescriptionDanish: "Stærk suppe med mør spareribs og lime og chili",
            productDescriptionEnglish: "Spicy soup with tender spareribs and fresh lime og chili",
            productPrice: 179,

        },
        {
            productID: 57,
            productThaiName: "ปลาทับทิมยำมะม่วง",
            productLatinName: "Pla Tab Tim Yam Ma-Muang",
            spiceLevel: 1,
            productDescriptionDanish: "Sprød stegt rød tilapia fisk toppet med frisk mango salat og friske chili",
            productDescriptionEnglish: "Crispy fried red tilapia fish topped with fresh mango salad and fresh chili",
            productPrice: 250,

        },
        {
            productID: 58,
            productThaiName: "ปลาทับทิมสามรส",
            productLatinName: "Pla Tab Tim Sam Rod",
            productDescriptionDanish: "Sprød stegt rød tilapia fisk i rød karrypasta, sur-sødsauce, basilikum, grønsager, cashewnødder og ananas",
            productDescriptionEnglish: "Crispy fried red tilapia fish in red curry paste, sweet and sour sauce, basil, vegetables, cashews and pineapple ",
            productPrice: 250,

        },
        {
            productID: 59,
            productThaiName: "ปลาทับทิมราดพริก",
            productLatinName: "Pla Tab Tim Raad Prik",
            spiceLevel: 3,
            productDescriptionDanish: "Sprød stegt rød tilapia fisk med hvidløg, chili, basilikum",
            productDescriptionEnglish: "Crispy fried red tilapia fish with garlic, chili, basil",
            productPrice: 250,

        },
    ]

export const noodlesSoups: MenuItemInterface [] =
    [
        {
            productID: 60,
            productThaiName: "สุกี้ยากี้",
            productLatinName: "Suki Yaki",
            productDescriptionDanish: "Glasnudelsuppe i suki yaki sauce, æg, grønsager og (  Kylling  /  Oksekød  /  svinekød  / Tofu  /    Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- )",
            productDescriptionEnglish: "Glass noodles in suki yaki sauce, egg, vegetables and ( Chicken / Beef / Pork / Tofu / Shrimps +20,- / Squid +20,- / Seafood +20,-)",
            productPrice: 120,

        },
        {
            productID: 61,
            productThaiName: "ก๋วยเตี๋ยวราดหน้า",
            productLatinName: "Rad Naa",
            productDescriptionDanish: "En tyk nudelsuppe med grønsager og ( Kylling / Oksekød / svinekød / Tofu / Rejer +20,- / Blæksprutte +20,- / Skaldyr +20,- )",
            productDescriptionEnglish: "A thick noodle soup with vegetables and ( Chicken / Beef / Pork / Tofu / Shrimps +20,- / Squid +20,- / Seafood +20,-)",
            productPrice: 120,

        },
        {
            productID: 62,
            productThaiName: "บะหมี่เกี้ยวน้ำหมูแดง",
            productLatinName: "Ba Mee Kiow Nam Moo Daeng",
            productDescriptionDanish: "Æggenudler og WonTon i en klar suppe med marineret rødt svinekød, bønnespirer, vandspinat, ristet hvidløg og koriander",
            productDescriptionEnglish: "Egg noodles and WonTon in a clear soup with marinated red roasted pork, bean sprouts, water spinach, roasted garlic and coriander",
            productPrice: 120,

        },
        {
            productID: 63,
            productThaiName: "ก๋วยเตี๋ยวเย็นตาโฟ",
            productLatinName: "Yen Ta Fo Talay",
            productDescriptionDanish: "Special skaldyr nudelsuppe med grønsager",
            productDescriptionEnglish: "Special seafood noodle soup with vegetables",
            productPrice: 140,

        },
        {
            productID: 64,
            productThaiName: "ก๋วยเตี๋ยวน้ำตก",
            productLatinName: "Khuay Thiew Nam Tok",
            productDescriptionDanish: "Klassisk thai nudelsuppe med bønnespirer, vandspinat, koriander, ristet hvidløg og ( Oksekød / svinekød )",
            productDescriptionEnglish: "Classic thai noodle soup with bean sprouts, water spinach, coriander, roasted garlic and ( Beef / Pork )",
            productPrice: 120,

        },
    ]

export const dessert: DessertInterface[] =
    [
        {
            productID: 65,
            productLatinName: "Ice Cream",
            productDescriptionDanish: "Is med flødeskum og Chokoladesauce",
            productDescriptionEnglish: "Ice cream topped with whipped cream and chocolate sauce",
            productPrice: 39,
        },
        {
            productID: 66,
            productLatinName: "Lava Cake",
            productDescriptionDanish: "Lava kage med is, flødeskum og Chokoladesauce",
            productDescriptionEnglish: "Lava cake with Ice cream on the side topped with whipped cream and chocolate sauce",
            productPrice: 45,
        },
        {
            productID: 67,
            productLatinName: "Pancake",
            productDescriptionDanish: "Pandekage med is, flødeskum Chokoladesauce og syltetøj",
            productDescriptionEnglish: "Pancake with Ice cream on the side topped with whipped cream chocolate sauce and jam",
            productPrice: 45,
        },
        {
            productID: 68,
            productLatinName: "Deep Fried Banana",
            productDescriptionDanish: "Friturestegt banan med is, flødeskum og Chokoladesauce",
            productDescriptionEnglish: "Deep fried banana with Ice cream on the side topped with whipped cream and chocolate sauce",
            productPrice: 50,
        },
        {
            productID: 69,
            productLatinName: "BananaSplit",
            productDescriptionDanish: "Banan med 3 kugler is med flødeskum og Chokoladesauce",
            productDescriptionEnglish: "Banana with 3 scoop of ice cream topped with whipped cream and chocolate sauce",
            productPrice: 45,
        }
    ]

export const extras: AccessoriesInterface[] = [
    {
        productLatinDanish: "Ris",
        productLatinEnglish: "Rice",
        price: 10
    },
    {
        productLatinDanish: "Nudler",
        productLatinEnglish: "Noodles",
        price: 20
    },
    {
        productLatinDanish: "Grønsager",
        productLatinEnglish: "Vegetables",
        price: 10
    },
    {
        productLatinDanish: "Jordnøddesauce",
        productLatinEnglish: "Peanut Sauce",
        price: 20
    },
    {
        productLatinDanish: "Sød chili sauce",
        productLatinEnglish: "Sweet chili sauce",
        price: 10
    },
    {
        productLatinDanish: "Cashewnødder",
        productLatinEnglish: "Cashewnuts",
        price: 20
    },
    {
        productLatinDanish: "Friske chili i fiskesauce",
        productLatinEnglish: "Fresh chili in fish sauce",
        price: 20
    }
]
