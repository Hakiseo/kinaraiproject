import {LitElement, html, customElement, query, PropertyValues, property, TemplateResult} from 'lit-element';
import {
    curryDishes, extras,
    firstCourses,
    friedRice,
    mainCourses, noodles, noodlesSoups,
    soups, specialDishes,
    specialDuckDishes,
    specialFriedPork, thaiSalad
} from "./menuItems";
import {AccessoriesInterface, DessertInterface, MenuItemInterface} from "./menuItemInterface";
import { repeat } from 'lit-html/directives/repeat';

@customElement('menu-section')
export class MenuSection extends LitElement {

    @property() scrollToElement: string = "home"
    @query("#soups") soups!: HTMLElement;
    @query("#starters") starters!: HTMLElement;
    @query("#mains") mains!: HTMLElement;
    @query("#extras") extras!: HTMLElement;
    @query("#desserts") dessert!: HTMLElement;

    @property({type: Boolean}) mobileTabletMode: boolean = false;

    constructor() {
        super();
    }

    protected updated(_changedProperties: PropertyValues) {
        super.updated(_changedProperties);
        if (_changedProperties.has("scrollToElement")) {
            switch (this.scrollToElement) {
                case "soups":
                    window.scrollTo({behavior: "smooth", top: this.soups.offsetTop - 60})
                    break;
                case "starters":
                    window.scrollTo({behavior: "smooth", top: this.starters.offsetTop - 60})
                    break;
                case "mains":
                    window.scrollTo({behavior: "smooth", top: this.mains.offsetTop - 60})
                    break;
                case "extras":
                    window.scrollTo({behavior: "smooth", top: this.extras.offsetTop - 60})
                    break;
                case "desserts":
                    window.scrollTo({behavior: "smooth", top: this.dessert.offsetTop - 60})
                    break;
                default:
                    break;
            }
        }
    }

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
    }

    renderChilis(chilis: number) {
      switch (chilis) {
          case 1:
              return html `
                  <i class="fas fa-pepper-hot" style="padding-right: 3px"></i>
              `
          case 2:
              return html `
                  <i class="fas fa-pepper-hot" style="padding-right: 3px"></i>
                  <i class="fas fa-pepper-hot" style="padding-right: 3px"></i>
              `
          case 3:
              return html `
                  <i class="fas fa-pepper-hot" style="padding-right: 3px"></i>
                  <i class="fas fa-pepper-hot" style="padding-right: 3px"></i>
                  <i class="fas fa-pepper-hot" style="padding-right: 3px"></i>
              `
          default:
              return ""
      }
  }

  renderMenuItem(item: MenuItemInterface) {
    return html `
        <p class="menuItemName"> ${item.productID}. ${item.productThaiName} ${item.productLatinName} ${item.spiceLevel ? this.renderChilis(item.spiceLevel) : "" } ${item.pieces ? html`<span> ${item.pieces }</span>` : ""} </p>
        <p class="menuItemPrice"> ${item.productPrice},- </p>
        <div class="productDescription">
            <p> ${item.productDescriptionDanish} </p>
            <p> <i> ${item.productDescriptionEnglish} </i> </p>
        </div>
    `
  }

    renderMenuTilbehør(item: AccessoriesInterface) {
        return html `
        <p class="menuItemName"> ${item.productLatinDanish} / ${item.productLatinEnglish} </p>
        <p class="menuItemPrice"> ${item.price},- </p>
    `
    }

    renderMenuDessert(item: DessertInterface) {
        return html `
        <p class="menuItemName"> ${item.productID}. ${item.productLatinName} ${item.productDescriptionDanish} </p>
        <p class="menuItemPrice"> ${item.productPrice},- </p>
        <div class="productDescription">
            <p> ${item.productDescriptionEnglish} </p>
        </div>
    `
    }

    renderUpperLeaf(): TemplateResult | void {
        return html `
            <div class="upperLeaf">
                <img src="/assets/images/leaf-300.png" loading="lazy" alt="golden leaf in upper left corner" class="upperLeafImg">
            </div>
        `
    }

    renderLowerLeaf(): TemplateResult | void {
        return html `
            <div class="lowerLeaf">
                <img src="/assets/images/leaf-300.png" loading="lazy" alt="golden leaf in lower right corner" class="lowerLeafImg">
            </div>
        `
    }

    render() {
      return html`
        <link rel="preload" as="style" href="global.css">
        <link rel="stylesheet" href="global.css">
        <link rel="preload" as="style" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
        ${this.renderUpperLeaf()}
        <div class="menuItems">
            <div class="menuItem">
                <h2 id="soups"> Supper / Soups </h2>
                ${repeat(soups, i => this.renderMenuItem(i))}
                <h2 id="starters"> Forretter / First Courses </h2>
                ${repeat(firstCourses, i => this.renderMenuItem(i))}
                <h2 id="mains"> Hovedretter / Main Courses </h2>
                ${repeat(mainCourses, i => this.renderMenuItem(i))}
                <h2 id="specialDucks"> Special Ande retter / Special Duck Dishes </h2>
                ${repeat(specialDuckDishes, i => this.renderMenuItem(i))}
                <h2 id="specialPorks"> Special Stegt Flæsk / Special Fried Pork </h2>
                ${repeat(specialFriedPork, i => this.renderMenuItem(i))}
                <h2 id="currys"> Karry Retter / Curry Dishes </h2>
                ${repeat(curryDishes, i => this.renderMenuItem(i))}
                <h2 id="friedRices"> Stegte Ris / Fried Rice </h2>
                ${repeat(friedRice, i => this.renderMenuItem(i))}
                <h2 id="noodles"> Nudler / Noodles </h2>
                ${repeat(noodles, i => this.renderMenuItem(i))}
                <h2 id="salads"> Thai Salater / Thai Salad </h2>
                ${repeat(thaiSalad, i => this.renderMenuItem(i))}
                <h2 id="specials"> Special Retter / Special Dishes </h2>
                ${repeat(specialDishes, i => this.renderMenuItem(i))}
                <h2 id="noodleSoups"> Nuddelsupper / Noodle soups </h2>
                ${repeat(noodlesSoups, i => this.renderMenuItem(i))}
                <h2 id="extras"> Ekstra Tilbehør / Extra Accessories </h2>
                ${repeat(extras,i => this.renderMenuTilbehør(i))}
            </div>
        </div>
        ${this.renderLowerLeaf()}
    `;
  }
}
