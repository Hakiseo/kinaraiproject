import {LitElement, html, customElement, query, PropertyValues, property} from 'lit-element';
import '../components/header';
import '../components/menuSection';
import '../components/openingHours';

@customElement('app-index')
export class AppIndex extends LitElement {
    @query('opening-hours') hours!: HTMLElement;
    @property() elementToScrollTo: string = "";
    @property({type: Boolean}) mobileTabletMode: boolean = false;
    @property({type: Boolean}) useHorizontalImage: boolean = false;
    @property({type: Number}) widthOfPicture: number = 485;

  constructor() {
    super();
      this.mobileTabletMode = window.innerWidth <= 768;
      this.useHorizontalImage = window.innerWidth/2 < this.widthOfPicture
      window.addEventListener("resize", () => {
          if (window.innerWidth <= 768 && !this.mobileTabletMode) {
              this.mobileTabletMode = true
              this.requestUpdate()
          }
          if (window.innerWidth > 768 && this.mobileTabletMode) {
              this.mobileTabletMode = false;
              this.requestUpdate()
          }

          if (window.innerWidth/2 < this.widthOfPicture && !this.useHorizontalImage) {
              this.useHorizontalImage = true;
              this.requestUpdate()
          }
          if (window.innerWidth/2 > this.widthOfPicture && this.useHorizontalImage) {
              this.useHorizontalImage = false;
              this.requestUpdate()
          }
      })
  }

  firstUpdated(_changedProperties: PropertyValues) {
    super.firstUpdated(_changedProperties);
  }

  determineScrollDestination(destination: string) {
      switch (destination) {
          case "home":
              window.scrollTo(0,0)
              break;
          case "hours":
              console.log("hours");
              window.scrollTo({behavior: "smooth", top: this.hours.offsetTop - 45})
              break;
          default:
              this.elementToScrollTo = destination;
              break;
      }
  }

  render() {
    return html`
      <link rel="preload" as="style" href="global.css">
      <link rel="stylesheet" href="global.css">
      <link rel="preload" as="style" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
      <div>
        <main>
            ${this.renderBanner()}
          <app-header .mobileTabletMode="${this.mobileTabletMode}" class="sticky" @scrollTo="${(e: CustomEvent) => this.determineScrollDestination(e.detail)}"></app-header>
          <menu-section .mobileTabletMode="${this.mobileTabletMode}" .scrollToElement="${this.elementToScrollTo}"></menu-section>
          <opening-hours .mobileTabletMode="${this.mobileTabletMode}" .useHorizontalImage="${this.useHorizontalImage}"></opening-hours>
          <footer>
            <a href="https://www.facebook.com/Kinaraithaicorner" target="_blank" rel="noopener" aria-label="Facebook"><i class="fa-2x fab fa-facebook"></i></a>
            <a href="https://www.instagram.com/kinarai.cph/" target="_blank" rel="noopener" aria-label="Instagram"><i class="fa-2x fab fa-instagram"></i></a>
            <p> Copyright © Kin Arai Thai Restaurant 2021. All Rights Reserved.  </p> 
          </footer>
        </main>
      </div>
    `;
  }

  renderBanner() {
      if (this.mobileTabletMode) {
          return html `
              <link rel="preload" as="image" href="/assets/images/Kin_Arai_Banner-768.png"/>
              <img src="/assets/images/Kin_Arai_Banner-768.png" alt="KinArai Logo Banner" class="banner">
          `
      }
      return html `
          <link rel="preload" as="image" href="/assets/images/Kin_Arai_Banner.png"/>
          <img src="/assets/images/Kin_Arai_Banner.png" alt="KinArai Logo Banner" class="banner">
      `
  }
}
